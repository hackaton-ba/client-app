#!/usr/bin/make -f

# RUN WHOLE PROCESS IN ONE SHELL
.ONESHELL:

################################################################################
################################################################################
# Variable definitions
################################################################################

# Are we running in an interactive shell? If so then we can use codes for
# a colored output
ifeq ("$(shell [ -t 0 ] && echo yes)","yes")
FORMAT_BOLD=\e[1m
FORMAT_RED=\033[0;31m
FORMAT_YELLOW=\033[0;33m
FORMAT_GREEN=\x1b[32;01m
FORMAT_RESET=\033[0m
else
FORMAT_BOLD=
FORMAT_RED=
FORMAT_YELLOW=
FORMAT_GREEN=
FORMAT_RESET=
endif

# Path to the echo binary. This is needed because on some systems there are
# multiple versions installed and the alias "echo" may reffer to something
# different.
ECHO=$(shell which echo -e)
OSECHOFLAG=-e
UNAME_S := $(shell uname -s)

ifeq ($(UNAME_S),Darwin)
	ECHO=echo
	OSECHOFLAG=
	FORMAT_BOLD=
endif

################################################################################
################################################################################
# Help and tool warmup
################################################################################

# RUN WHOLE PROCESS IN ONE SHELL
.ONESHELL:

# BY DEFAULT SHOW HELP
.DEFAULT_GOAL:=help
.DEFAULT:
	@$(MAKE) help
help:
	@awk 'BEGIN {FS = ":.*##";} /^[a-zA-Z_\-\/]+:.*?##/ { printf "  $(FORMAT_YELLOW)make %-20s\033[0m %s\n", $$1, $$2 } /^##@/ { printf "\n\n\033[1m%s\033[0m\n\n", substr($$0, 5) } ' $(MAKEFILE_LIST)
	@$(ECHO) $(OSECHOFLAG) "\n\n"


start: ## Start Docker containers
	@$(ECHO) $(OSECHOFLAG) "\n\n$(FORMAT_YELLOW)Starting services$(FORMAT_RESET)\n"
	docker-compose up -d -V --force-recreate --remove-orphans --build
	@$(ECHO) $(OSECHOFLAG) "\n\n$(FORMAT_YELLOW)Application running at http://localhost:3000$(FORMAT_RESET)\n"

stop: ## Stop Docker containers
	@$(ECHO) $(OSECHOFLAG) "\n\n$(FORMAT_YELLOW)Stoping services$(FORMAT_RESET)\n"
	docker-compose down -v

sh: ## Log into the main application container
	@$(ECHO) $(OSECHOFLAG) "\n\n$(FORMAT_YELLOW)Logging into node container$(FORMAT_RESET)\n"
	docker-compose exec app sh

logs: ## Display all logs (follow mode)
	@$(ECHO) $(OSECHOFLAG) "\n\n$(FORMAT_YELLOW)Showing all logs$(FORMAT_RESET)\n"
	docker-compose logs -f --tail=100 app

generate:
	wget http://localhost/docs/api-docs.json -O ./openapi/api-docs.json
	npx @rtk-query/codegen-openapi ./openapi/openapi-config.ts
	rm ./openapi/api-docs.json


################################################################################
################################################################################
# Deployment
################################################################################
# .PHONY: release
# release:
# 	python3 ./bin/git_tag.py $(filter-out $@,$(MAKECMDGOALS))

# major:
# 	@echo "Makefile: Increasing major version done."
# minor:
# 	@echo "Makefile: Increasing minor version done."
# patch:
# 	@echo "Makefile: Increasing patch version done."