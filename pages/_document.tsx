import Document, { Html, Head, Main, NextScript } from "next/document";
class MyDocument extends Document {
    static async getInitialProps(ctx: any) {
        const initialProps = await Document.getInitialProps(ctx);
        return { ...initialProps };
    }

    render() {
        return (
            <Html lang="sk" className="scroll-smooth">
                <Head />
                <body className="text-dark scroll-smooth">
                    <Main />
                    <NextScript />

                </body>
            </Html>
        );
    }
}

export default MyDocument;
