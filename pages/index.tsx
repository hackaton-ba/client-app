import AppLayout from "@/common/AppLayout";

export default function Index() {
  return (
    <>
    <div className="container pt-24 md:pt-36 mx-auto flex flex-wrap flex-col md:flex-row items-center">
    
      <div className="flex flex-col w-full xl:w-2/5 justify-center lg:items-start overflow-y-hidden">
        <h1 className="my-4 text-3xl md:text-5xl text-white opacity-75 font-bold leading-tight text-center md:text-left px-4">
          Novinka v Starom Meste v Bratislave
          <span className="bg-clip-text text-transparent bg-gradient-to-r from-green-400 via-pink-500 to-purple-500">
             
          </span>
        </h1>
        <p className="leading-normal text-base md:text-2xl mb-8 text-center md:text-left px-4">
          
        </p>

        <h4 className="px-8 pt-6 pb-8 mb-4">
          <div className="mb-4">
        Naša nová automatizovaná rampa v starom meste prináša pohodlný 
        a bezpečný spôsob prechodu do historického jadra. S modernou 
        technológiou umožňuje jednoduchý prístup pre všetkých, bez ohľadu 
        na ich pohyblivosť. Naša webová platforma vám tiež umožňuje jednoducho
        získať povolenie na využívanie rampy, čím robíme proces ešte pohodlnejším
        a prístupnejším pre všetkých našich obyvateľov a návštevníkov.
      
          </div>
        </h4>
      </div>

      <div className="w-full xl:w-3/5 p-12 overflow-hidden">
        <img className="mx-auto w-full md:w-4/5 transform -rotate-6 transition hover:scale-105 duration-700 ease-in-out hover:rotate-6" src="images/Bratislava-staré-mesto-1.jpg" />
      </div>
      
      <div className="w-full pt-16 pb-6 text-sm text-center md:text-left fade-in">
        <a className="text-gray-500 no-underline hover:no-underline" href="#">&copy;Copyright 2023</a>
      </div>
    </div>
    </>
  )
}

Index.getLayout = function getLayout(page: any) {
  return <AppLayout>{page}</AppLayout>;
};
