import AppLayout from "@/common/AppLayout";

export default function Dokumenty() {
    return (
    <>
    <div className="min-h-screen p-6 bg-gray-100 flex items-center justify-center">
  <div className="container max-w-screen-lg mx-auto">
    <div>
      <h2 className="font-semibold text-xl text-gray-600">Formular</h2>
      <p className="text-gray-500 mb-6"></p>

      <div className="bg-white rounded shadow-lg p-4 px-4 md:p-8 mb-6">
        <div className="grid gap-4 gap-y-2 text-sm grid-cols-1 lg:grid-cols-3">
          <div className="text-gray-600">
            <p className="font-medium text-lg">Osobné údaje</p>
            <p>Prosím vyplňte.</p>
          </div>


          {/* Meno / Priezvisko */}
          <div className="lg:col-span-2 text-gray-600">
            <div className="grid gap-4 gap-y-2 text-sm grid-cols-1 md:grid-cols-5">
              <div className="md:col-span-5">
                <div className="flex flex-row gap-4 w-full">
                  <div className="w-full">
                  <label htmlFor="first_name" className="text-gray-600">Meno</label>
                  <input type="text" name="first_name" id="first_name" className="h-10 border mt-1 rounded px-4 w-full bg-gray-50" value="" placeholder="Meno" />
                  </div>
                  <div className="w-full">
                  <label htmlFor="last_name" className="text-gray-600">Priezvisko</label>
                  <input type="text" name="last_name" id="last_name" className="h-10 border mt-1 rounded px-4 w-full bg-gray-50" value="" placeholder="Priezvisko" />
                  </div>
                </div>
              </div>
              
              {/* Widlo pravnicke osoby / Adresa trvaleho pobytu fyzickej osoby (ziadatel) */}
              <div className="md:col-span-5 text-gray-600">
                <label htmlFor="address">Sídlo právnickej osoby / Adresa trvalého pobytu fyzickej osoby (žiadateľ)</label>
                <input type="text" name="address" id="address" className="h-10 border mt-1 rounded px-4 w-full bg-gray-50" value="" placeholder="Adresa" />
              </div>

              {/* ICO pravnickej osoby / Rodne cislo fyzickej osoby (ziadatel) */}
              <div className="md:col-span-5 text-gray-600">
                <label htmlFor="ico">IČO právnickej osoby/Rodné číslo fyzickej osoby (žiadateľ)</label>
                <input type="text" name="ico" id="ico" className="h-10 border mt-1 rounded px-4 w-full bg-gray-50" value="" placeholder="12345678" />
              </div>

              {/* Emailova adresa / Telefonne cislo */}
              <div className="md:col-span-5 text-gray-600">
              <div className="grid gap-4 gap-y-2 text-sm grid-cols-1 md:grid-cols-5">
                <div className="md:col-span-5">
                  <div className="flex flex-row gap-4 w-full">
                    <div className="w-full">
                      <label htmlFor="email" className="text-gray-600">Emailová adresa</label>
                      <input type="text" name="email" id="email" className="h-10 border mt-1 rounded px-4 w-full bg-gray-50" value="" placeholder="meno@gmail.com"/>
                    </div>
                    <div className="w-full">
                      <label htmlFor="phone_number" className="text-gray-600">Telefónne číslo</label>
                      <input type="text" name="phone_number" id="phone_number" className="h-10 border mt-1 rounded px-4 w-full bg-gray-50" value="" placeholder="+421 *** ***" />
                    </div>
                  </div>
                </div>
                </div>
                </div>
                
                {/*  Evidencne cislo motoroveho vozdila */}
                <div className="md:col-span-5 text-gray-600">
                <label htmlFor="evidence">Evidenčné číslo motorového vozidla</label>
                <input type="text" name="evidence" id="evidence" className="h-10 border mt-1 rounded px-4 w-full bg-gray-50" value="" placeholder="ŠPZ"/>
              </div>

              {/*  Tovarenska znacka/typ */}
              <div className="md:col-span-5 text-gray-600">
                <label htmlFor="type">Továrenská značka/typ</label>
                <input type="text" name="type" id="type" className="h-10 border mt-1 rounded px-4 w-full bg-gray-50" value="" placeholder="Továrenská značka"/>
              </div>


              {/* Celkova hmotnost motoroveho vozidla podla technickeho preukazu */}
              <div className="md:col-span-5 text-gray-600">
                <label htmlFor="hmotnost">Celková hmotnosť motorového vozidla podľa technického preukazu</label>
                <input type="text" name="hmotnost" id="hmotnost" className="h-10 border mt-1 rounded px-4 w-full bg-gray-50" value="" placeholder="t"/>
              </div>

              {/* Ucel vjazdu do historickej casti */}
              <div className="md:col-span-5 text-gray-600">
                <label htmlFor="entry">Účel vjazdu do historickej časti</label>
                <input type="text" name="entry" id="entry" className="h-10 border mt-1 rounded px-4 w-full bg-gray-50" value="" placeholder="Účel vjazdu"/>
              </div>


              {/* Miesto zastavenia v historickej casti */}
              <div className="md:col-span-5 text-gray-600">
                <label htmlFor="place_to_stay">Miesto zastavenia v historickej časti</label>
                <input type="text" name="place_to_stay" id="place_to_stay" className="h-10 border mt-1 rounded px-4 w-full bg-gray-50" value="" placeholder="Miesto zastavenia"/>
              </div>

              {/* Miesto parkovania v historickej casti */}
              <div className="md:col-span-5 text-gray-600">
                <label htmlFor="place_to_park">Miesto parkovania v historickej časti</label>
                <input type="text" name="place_to_park" id="place_to_park" className="h-10 border mt-1 rounded px-4 w-full bg-gray-50" value="" placeholder="Miesto parkovania"/> 
              </div>


                {/* Pocet dni vjazdu / Od / Do */}
                <div className="md:col-span-5 text-gray-600">
              <div>
                  <div className="flex flex-row gap-4 w-full">
                    <div className="w-full">
                      <label htmlFor="how_many_days" className="text-gray-600">Počet dní vjazdu</label>
                      <input type="text" name="how_many_days" id="how_many_days" className="h-10 border mt-1 rounded px-4 w-full bg-gray-50" value="" placeholder="Počet dní vjazdu" />
                    </div>
                    <div className="w-full">
                      <label htmlFor="from_time" className="text-gray-600">Od</label>
                      <input type="text" name="from_time" id="from_time" className="h-10 border mt-1 rounded px-4 w-full bg-gray-50" value="" placeholder="Od"/>
                    </div>
                    <div className="w-full">
                      <label htmlFor="to_time" className="text-gray-600">Do</label>
                      <input type="text" name="to_time" id="to_time" className="h-10 border mt-1 rounded px-4 w-full bg-gray-50" value="" placeholder="Do"></input>
                    </div>  
                  </div>
                </div>
                </div>
                </div>
                

              <div className="md:col-span-5 text-gray-600">
                <div className="inline-flex items-center class-gray-600">
                  <input type="checkbox" name="billing_same" id="billing_same" className="h-10 class-gray-600"/>

                  <label htmlFor="billing_same" className="ml-2 text-gray-600">Suhlasim so spracovanim osobnych udajov.</label>
                </div>
              </div>

              <div className="md:col-span-5 text-gray-600">
                <div>
                  <input type="checkbox" name="billing_same2" id="billing_same2" className="h-10 text-gray-600"/>

                  <label htmlFor="billing_same" className="ml-2 text-gray-600">Vyhlasujem, že všetky údaje uvedené v žiadosti sú pravdivé, úplné a správne a som si vedomý právnych následkov v prípade uvedenia nepravdivých údajov.</label>
                </div>
              </div>
      
              <div className="md:col-span-5 text-right">
                <div className="inline-flex items-end">
                  <button className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded">Odoslat</button>
                </div>
              </div>

            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</>
    )
}


Dokumenty.getLayout = function getLayout(page: any) {
    return <AppLayout>{page}</AppLayout>;
  };